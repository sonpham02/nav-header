import React from "react";
import "./Nav.css";

export class Nav extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            pathname: ''
        }
    }

    render () {
        const {pathname} = this.state;

        return (
            <ul className="nav nav-wrapper">
                <li className={`nav-item ${pathname.indexOf('active') > -1 ? `active` : ``}`}>
                    <a className="nav-link" onClick={() => this.setState({pathname: "active"})}>Active</a>
                </li>
                <li className={`nav-item dropdown ${pathname.indexOf('multi') > -1 ? `active` : ``}`}>
                    <a className="nav-link dropdown-toggle"
                       role="button"
                       aria-haspopup="true"
                       aria-expanded="false"
                    >
                        Dropdown Multi
                    </a>
                    <div className={`dropdown-menu dropdown-multi`}>
                        <div className="container">
                            <div className="row">
                                <div className="col">
                                    <div className="title">
                                        <span>Section header</span>
                                    </div>

                                    <a className="dropdown-item" onClick={() => this.setState({pathname: "multi"})}>Action</a>
                                    <a className="dropdown-item" onClick={() => this.setState({pathname: "multi"})}>Another action</a>
                                    <a className="dropdown-item" onClick={() => this.setState({pathname: "multi"})}>Something else here</a>
                                </div>
                                <div className="col">
                                    <div className="title">
                                        <span>Section header</span>
                                    </div>

                                    <a className="dropdown-item" onClick={() => this.setState({pathname: "multi"})}>Action</a>
                                    <a className="dropdown-item" onClick={() => this.setState({pathname: "multi"})}>Another action</a>
                                    <a className="dropdown-item" onClick={() => this.setState({pathname: "multi"})}>Something else here</a>
                                </div>
                            </div>

                            <br/>

                            <div className="row">
                                <div className="col">
                                    <div className="title">
                                        <span>Section header</span>
                                    </div>

                                    <a className="dropdown-item" onClick={() => this.setState({pathname: "multi"})}>Action</a>
                                    <a className="dropdown-item" onClick={() => this.setState({pathname: "multi"})}>Another action</a>
                                    <a className="dropdown-item" onClick={() => this.setState({pathname: "multi"})}>Something else here</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>

                <li className={`nav-item dropdown ${pathname.indexOf('dropdown') > -1 ? `active` : ``}`}>
                    <a className="nav-link dropdown-toggle" role="button"
                       aria-haspopup="true" aria-expanded="false">Dropdown</a>
                    <div className="dropdown-menu">
                        <a className="dropdown-item" onClick={() => this.setState({pathname: "dropdown"})}>Action</a>
                        <a className="dropdown-item" onClick={() => this.setState({pathname: "dropdown"})}>Another action</a>
                        <a className="dropdown-item" onClick={() => this.setState({pathname: "dropdown"})}>Something else here</a>
                    </div>
                </li>
            </ul>
        )
    }
}
